<?php


/**
 * Display current PEAR status.
 */
function go_pear_settings() {
  if(!go_pear_check_installed()) {
    return t('PEAR has not been downloaded. Please !link.', array('!link' => l(t('download and install it'), 'admin/settings/go-pear/download')));
  }
  else if(!go_pear_is_clean()) {
    return t('One or more previously PEAR temporary files need to be removed. Please !link.', array('!link' => l(t('remove them'), 'admin/settings/go-pear/clean')));
  }
  return t('PEAR is correctly installed and cleaned. Nothing to be done.');
}

/**
 * Check if PEAR is already installed.
 * 
 * @return bool
 */
function go_pear_check_installed() {
  $path = drupal_get_path('module', 'go_pear');
  if(file_exists($path . '/' . 'PEAR/PEAR.php')) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Check if there are still temporary files.
 * 
 * @return bool
 */
function go_pear_is_clean() {
  $path = drupal_get_path('module', 'go_pear');
  foreach(go_pear_downloaded_files() as $file) {
    if(file_exists($path . '/' . $file)) {
      return FALSE;
    }
  }
  return TRUE;
}

/**
 * Download go-pear.php file.
 */
function go_pear_download() {
  if(go_pear_check_installed()) {
    drupal_goto('admin/settings/go-pear');
  }
  $url = 'http://pear.php.net/go-pear';
  $file_path = drupal_get_path('module', 'go_pear') . '/go-pear.php';
  $file = fopen($file_path, 'w');
  $file_contents = go_pear_get_file_contents($url);
  fwrite($file, $file_contents);
  fclose($file);
  drupal_goto($file_path);
}

/**
 * Get the content of the URL $url and returns it into a string.
 * 
 * @return string
 * @param string $url
 * @desc Return string content from a remote file
 */
function go_pear_get_file_contents($url)
{
  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_HEADER, 0);

  ob_start();

  curl_exec($ch);
  curl_close($ch);
  $string = ob_get_contents();

  ob_end_clean();

  return $string;    
}

/**
 * Review and clean unnecessary downloaded files.
 */
function go_pear_clean() {
  if(go_pear_is_clean()) {
    drupal_goto('admin/settings/go-pear');
  }
  $path = drupal_get_path('module', 'go_pear');
  $rows = array();
  foreach(go_pear_downloaded_files() as $file) {
    $cols = array($file);
    if(file_exists($path . '/' . $file)) {
      $cols[] = t('Yes');
    }
    else {
      $cols[] = t('No');
    }
    $rows[] = $cols;
  }
  $rows[] = array('', drupal_get_form('go_pear_clean_form'));
  $header = array(
    t('File'),
    t('Exists'),
  );
  return theme_table($header, $rows);
}

/**
 * Form to remove temporary files.
 */
function go_pear_clean_form() {
  $form['remove'] = array(
    '#value' => t('Remove'),
    '#type' => 'submit',
  );
  return $form;
}

/**
 * Form submission to remove temporary files.
 */
function go_pear_clean_form_submit() {
  $path = drupal_get_path('module', 'go_pear');
  foreach(go_pear_downloaded_files() as $file) {
    if(file_exists($path . '/' . $file)) {
      unlink($path . '/' . $file);
    }
  }
  drupal_set_message(t('PEAR files cleaned.'));
  drupal_goto('admin/settings/go-pear');
}

/**
 * Get temporary files downloaded by go-pear.php.
 * 
 * @return array
 */
function go_pear_downloaded_files() {
  return array(
    'go-pear.php',
    'index.php',
    'pear.bat',
    'pear.conf',
    'peardev.bat',
    'pecl.bat',
  );
}